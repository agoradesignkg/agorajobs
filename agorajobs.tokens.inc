<?php

/**
 * @file
 * Builds placeholder replacement tokens for agorajobs module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function agorajobs_token_info() {
  $types = [
    'job-offers' => [
      'name' => t("Job offers"),
      'description' => t("Special agorajobs module tokens."),
    ],
  ];

  $agorajobs = [
    'list' => [
      'name' => t("Job offers list"),
      'description' => t("Lists currently active job offers."),
    ],
  ];

  return [
    'types' => $types,
    'tokens' => [
      'job-offers' => $agorajobs,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function agorajobs_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();
  /** @var \Drupal\Core\Render\RendererInterface $renderer */
  $renderer = \Drupal::service('renderer');

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
  }
  $replacements = [];

  if ($type == 'job-offers') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'list':
          $limit = 5;
          $render_array = agorajobs_render_job_offer_overview_list($limit, TRUE, $url_options);
          $replacements[$original] = $renderer->renderRoot($render_array);
          break;
      }
    }

    if ($job_offer_tokens = $token_service->findWithPrefix($tokens, 'list')) {
      foreach ($job_offer_tokens as $limit => $original) {
        $render_array = agorajobs_render_job_offer_overview_list($limit, TRUE, $url_options);
        $replacements[$original] = $renderer->renderRoot($render_array);
      }
    }
  }

  return $replacements;
}
