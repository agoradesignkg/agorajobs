<?php

namespace Drupal\agorajobs\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines the job offer page route cache context.
 *
 * Cache context ID: 'route.job_offer'.
 */
class JobOfferRouteCacheContext implements CacheContextInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new JobOfferRouteCacheContext object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Job offer page route context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $current_route_name = $this->routeMatch->getRouteName();
    if ($current_route_name === 'entity.node.canonical') {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $this->routeMatch->getParameter('node');
      return (int) $node->bundle() === 'job_offer';
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
