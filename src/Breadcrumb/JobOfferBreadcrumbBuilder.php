<?php

namespace Drupal\agorajobs\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the job offer breadcrumb builder.
 */
class JobOfferBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() !== 'entity.node.canonical') {
      return FALSE;
    }
    /** @var \Drupal\node\NodeInterface $node */
    $node = $route_match->getParameter('node');
    if ($node->bundle() === 'job_offer') {
      return TRUE;
    }
    if ($overview_nid = agorajobs_get_jobs_overview_nid()) {
      return ((int) $node->id()) === $overview_nid;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $route_match->getParameter('node');

    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheableDependency($node);
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    if ($overview_url = agorajobs_get_jobs_overview_url()) {
      $breadcrumb->addLink(Link::fromTextAndUrl($this->t('Careers'), $overview_url));
    }

    // Check, if the given node is a job offer - otherwise it's the overview.
    if ($node->bundle() === 'job_offer') {
      $breadcrumb->addLink(Link::fromTextAndUrl($node->label(), $node->toUrl()));
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);
    return $breadcrumb;
  }

}
